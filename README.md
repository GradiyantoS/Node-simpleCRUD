# Node SimpleCRUD

simple crud api

## Getting started

I already deploy the node.js using PM2 on  AWS EC 2so the web app can run without running this server locally, but if something happen, you could run this server first in local.. 
this is Node.JS with MongoDB , so if you don't have mongo, you could download it from [mongoDB](https://www.mongodb.com/try/download/community).

## Step to recreate

use the following command step to launch the server:

```
git clone https://gitlab.com/GradiyantoS/Node-simpleCRUD.git
cd Node-simpleCRUD
npm install
```

after that run the command :
```
node app
```

and if success, you could open it from 
http://localhost:8000/
