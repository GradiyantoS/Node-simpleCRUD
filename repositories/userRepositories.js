const mongoose = require('mongoose')
const User = require('../models/user')

const getAll = ()=>{
    return User.find({});
}


const getById = (id)=>{
    return User.findById(id)
}
const insert = (user)=>{
    return user.save();
}


const update = (id,user)=>{
    return User.findByIdAndUpdate(id,user)
}


const remove = (id)=>{
    return User.findByIdAndDelete(id)
}
module.exports = {
    insert,
    update,
    remove,
    getAll,
    getById
}