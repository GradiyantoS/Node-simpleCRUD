const express = require('express')
var router = express()
const userController = require('../controllers/user')

const bodyparser = require('body-parser');

router.use(bodyparser.json())
router.post('/',userController.create)
router.get('/',userController.view)
router.get('/:id',userController.getById)
router.put('/:id',userController.update)
router.delete('/:id',userController.remove)

module.exports = router