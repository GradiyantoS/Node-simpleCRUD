
const express = require('express');
const https = require('https')
const cors = require('cors');
const mongoose = require('mongoose');
const userRouter = require('./routes/user')
const fs = require('fs')
const port = 8000

var app = express();
app.use(cors())

//MongoDB connection
mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true});
mongoose.connection.once('open',function(){
  console.log('Database connected Successfully');
}).on('error',function(err){
  console.log('Error', err);
})

//routes
app.use('/users',userRouter);

//HTTPS Server
// const httpsOptions = {
  // key:fs.readFileSync('./key.pem'),
  // cert:fs.readFileSync('./cert.pem')
// }

// const server = https.createServer(httpsOptions,app).listen(port,()=>{
//   console.log('server running at '+port)
// })

//Server
app.listen(8000,function(){
  console.log('Server is Up')
})