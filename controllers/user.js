const userService = require('../services/userService')
const User = require('../models/user')

async function  create(req,res,next){
  let name = req.body.name;
  let email = req.body.email;
  let mobile = req.body.mobile;
  let user = new User({
    name,
    email,
    mobile
  })

  try{
    data= await userService.createUser(user)
    res.send(data).status(200)
  }catch(e){
    res.sendStatus(500) 
  }
}


async function view(req,res,next){
  try{
    data= await userService.getUsers()
    res.send(data).status(200)
  }catch(e){
    res.sendStatus(500) 
  }
}

async function getById(req,res,next){
  const id = req.params.id
  try{
    data= await userService.getUserById(id)
    res.send(data).status(200)
  }catch(e){
    res.sendStatus(500) 
  }
}

async function update(req,res,next){
  const id = req.params.id
  try{
    data= await userService.updateUser(id,req.body)
    res.send(data).status(200)
  }catch(e){
    res.sendStatus(500) 
  }
}

async function remove(req,res,next){
  const id = req.params.id
  try{
    data= await userService.deleteUser(id)
    res.send({success: 'delete successfully'}).status(200)
  }catch(e){
    res.sendStatus(500) 
  }
}

module.exports.remove = remove
module.exports.create = create
module.exports.view = view
module.exports.getById = getById
module.exports.update = update
