
const {insert,getAll,getById,update,remove} = require('../repositories/userRepositories')

const createUser = async (user)=>{
    try{
        return await insert(user)
    }catch(e){
        throw new Error(e.message)
    }
}

const updateUser = async (id,user)=>{
    try{
        return await update(id,user)
    }catch(e){
        throw new Error(e.message)
    }
}
const getUsers = async ()=>{
    try{
        return await getAll()
    }catch(e){
        throw new Error(e.message)
    }
}


const getUserById = async (id)=>{
    try{
        return await getById(id)
    }catch(e){
        throw new Error(e.message)
    }
}
const deleteUser = async (id)=>{
    try{
        return await remove(id)
    }catch(e){
        throw new Error(e.message)
    }
}
module.exports={
    createUser,
    getUsers,
    getUserById,
    updateUser,
    deleteUser
}